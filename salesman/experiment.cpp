#include <iostream>
#include <cmath>
#include "experiment.h"



CExperiment::CExperiment():currentGraph(0){};


void CExperiment::resetGraph(size_t size) {
    currentGraph = CGraph(size);
}

void CExperiment::run(size_t graphSize, int repeat) {
    for(int i = 0; i < repeat; ++i){
        resetGraph(graphSize);
        MstResults.push_back(currentGraph.getEulerianPathLen());
        BestResults.push_back(currentGraph.getBestPathLen());
    }
}

double CExperiment::getAverage() {
    if(MstResults.size() == 0)
        return 0;
    double sum = 0;
    for(int i = 0; i < MstResults.size(); ++i) {
        sum += (MstResults[i] / BestResults[i]);
    }
    return sum/MstResults.size();
}

double CExperiment::getDeviation() {
    if(MstResults.size() == 0)
        return 0;
    double result = 0;
    for(int i = 0; i < MstResults.size(); ++i){
        double average = getAverage();
        for(int i = 0; i < MstResults.size(); ++i){
            result += pow(average-(MstResults[i] / BestResults[i]), 2);
        }
    }

    return sqrt(result/MstResults.size());
}
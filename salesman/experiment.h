#ifndef EXPERIMENT_H
#define EXPERIMENT_H


#include "graph.h"
#include <vector>
#include <map>


class CExperiment{
    CGraph currentGraph;
    void resetGraph(size_t size);
    std::vector<double> MstResults;
    std::vector<double> BestResults;
public:
    CExperiment();
    void run(size_t graphSize, int repeat);
    double getAverage();
    double getDeviation();

};

#endif //EXPERIMENT_H
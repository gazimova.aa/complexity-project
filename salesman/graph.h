#ifndef GRAPH_H
#define GRAPH_H

#include <vector>

struct CVertex{
    double x;
    double y;
    CVertex(double xValue, double yValue);
};

struct CEdge{
    int vertex1;
    int vertex2;
    double weight;
    CEdge(int vertex1Value, int vertex2Value, double weightValue);
};

class CGraph{
    std::vector<CVertex> vertices;
public:
    explicit CGraph(size_t size);
    std::vector<CVertex> getVertices();
    std::vector<CEdge> getEdges();
    std::vector<CEdge> getMst();
    std::vector<CEdge> getOddMatching();
    double getBestPathLen();
    double getEulerianPathLen();

};

#endif // GRAPH_H
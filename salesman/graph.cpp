#include "graph.h"
#include <cmath>
#include <algorithm>
#include <iostream>

CVertex::CVertex(double xValue, double yValue) {
    x = xValue;
    y = yValue;
}

CEdge::CEdge(int vertex1Value, int vertex2Value, double weightValue) {
    vertex1 = vertex1Value;
    vertex2 = vertex2Value;
    weight = weightValue;
}

bool operator<(const CEdge& edge1, const CEdge& edge2){
    return (edge1.weight < edge2.weight) ||
           (edge1.weight == edge2.weight && edge1.vertex1 < edge2.vertex1) ||
           (edge1.weight == edge2.weight && edge1.vertex1 == edge2.vertex1 &&
            edge1.vertex2 <edge2.vertex2);
}

double distance(CVertex vertex1, CVertex vertex2){
    return  sqrt(pow(vertex1.x-vertex2.x,2) + pow(vertex1.y-vertex2.y,2));
}

CGraph::CGraph(size_t size) {
    size_t counter = 0;
    while (counter  < size){
        double value1 = 2.0 * ((double)rand() / (RAND_MAX)) - 1.0;
        double value2 = 2.0 * ((double)rand() / (RAND_MAX)) - 1.0;
        double s = value1*value1 + value2*value2;
        if(s > 0 && s <= 1){
            counter++;
            double resValue1 = value1 * sqrt(-2*log(s)/s);
            double resValue2 = value2 * sqrt(-2*log(s)/s);
            vertices.push_back(CVertex(resValue1, resValue2));
        }
    }
}

std::vector<CVertex> CGraph::getVertices() {
    return vertices;
}

std::vector<CEdge> CGraph::getEdges() {
    std::vector<CEdge> result;
    for(size_t i = 0; i < vertices.size()-1; ++i){
        for(size_t j = i+1; j < vertices.size(); ++j){
            result.push_back(CEdge(i,j,distance(vertices[i], vertices[j])));
        }
    }

    return result;
}


int findParent(std::vector<int> &parent, int vert){
    if(parent[vert] == vert){
        return vert;
    }
    return parent[vert] = findParent(parent, parent[vert]);
}


void mergeComponents(std::vector<int> &parent, std::vector<int> &rank,
                     int vert1, int vert2) {
    vert1 = findParent(parent, vert1);
    vert2 = findParent(parent, vert2);
    if(vert1 != vert2){
        if(rank[vert1] < rank[vert2]) {
            std::swap(vert1, vert2);
        }
        parent[vert2] = vert1;
        if(rank[vert1] == rank[vert2]){
            ++rank[vert1];
        }
    }
}


std::vector<CEdge> CGraph::getMst() {
    auto edges = getEdges();
    std::vector<CEdge> result;
    std::vector<int> parent(vertices.size());
    std::vector<int> rank(vertices.size(),0);
    for(int vert = 0; vert < vertices.size(); ++vert){
        parent[vert] = vert;
    }
    std::sort(edges.begin(),edges.end());
    for(auto edge : edges){
        if(findParent(parent,edge.vertex1) != findParent(parent,edge.vertex2)){
            result.push_back(edge);
            mergeComponents(parent, rank, edge.vertex1, edge.vertex2);
        }
    }

    return result;
}



double CGraph::getBestPathLen() {
    double result;
    std::vector<int> path(vertices.size()-1);
    for(int i = 0; i < vertices.size()-1; ++i){
        path[i] = i+1;
    }

    result = distance(vertices[0],vertices[path[0]]) +
             distance(vertices[0],vertices[path[vertices.size()-2]]);
    for(int i = 0; i < path.size()-1; ++i){
        result += distance(vertices[path[i]],vertices[path[i+1]]);
    }

    while(std::next_permutation(path.begin(),path.end())){
        double cur_result = distance(vertices[0],vertices[path[0]]) +
                            distance(vertices[0],vertices[path[vertices.size()-2]]);
        for(int i = 0; i < path.size()-1; ++i){
            cur_result += distance(vertices[path[i]],vertices[path[i+1]]);
        }

        result = std::min(result,cur_result);
    }
    return result;
}

std::vector<CEdge> CGraph::getOddMatching() {
    std::vector<int> degree(vertices.size(),0);
    std::vector<CEdge> edges;
    for(auto edge: getMst()){
        ++degree[edge.vertex1];
        ++degree[edge.vertex2];
    }

    for(auto edge: getEdges()){
        if(degree[edge.vertex1]%2 && degree[edge.vertex2]%2){
            edges.push_back(edge);
        }
    }

    return edges;
}


double CGraph::getEulerianPathLen() {
    auto edges = getMst();
    double result = 0;
    auto addEdges = getOddMatching();
    std::vector<int> degree(vertices.size(), 0);
    for (auto edge: edges) {
        ++degree[edge.vertex1];
        ++degree[edge.vertex2];
        result += edge.weight;
    }
    std::sort(addEdges.begin(),addEdges.end());
    for (auto edge: addEdges) {
        if (degree[edge.vertex1]%2 && degree[edge.vertex2]%2 &&
            std::lower_bound(edges.begin(), edges.end(), edge) == edges.end()) {
            result += edge.weight;
            degree[edge.vertex1]++;
            degree[edge.vertex2]++;
        }
    }

    for (auto edge: addEdges) {
        if (degree[edge.vertex1]%2 && degree[edge.vertex2]%2) {
            result += edge.weight;
            degree[edge.vertex1]++;
            degree[edge.vertex2]++;
        }
    }



    return result;

}
#include <iostream>
#include <vector>
#include "experiment.h"


int main() {
    CExperiment experiment;
    for(int i = 2; i <= 10; ++i){
        std::cout << "Количество вершин: " << i << std::endl;
        experiment.run(i, 50);
        std::cout << "Среднее значение качества приближения: " << experiment.getAverage() << std::endl;
        std::cout << "Cреднеквадратичное отклонение качества приближения: "
                  << experiment.getDeviation() << std::endl;
        std::cout << std::endl;
    }

    return 0;
}